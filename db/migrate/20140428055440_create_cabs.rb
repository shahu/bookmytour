class CreateCabs < ActiveRecord::Migration
  def change
    create_table :cabs do |t|
      t.string :cab_name
      t.string :cab_type
      t.string :cab_number
      t.float :cab_rate
      t.integer :cab_running
      t.string :cab_status
      t.string :cab_company

      t.timestamps
    end
  end
end
