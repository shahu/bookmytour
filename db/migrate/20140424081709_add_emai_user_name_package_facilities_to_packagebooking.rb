class AddEmaiUserNamePackageFacilitiesToPackagebooking < ActiveRecord::Migration
  def change
    add_column :packagebookings, :user_name, :string
    add_column :packagebookings, :package, :string
    add_column :packagebookings, :facilities, :text
  end
end
