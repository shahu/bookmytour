class RemovePasswordFromUser < ActiveRecord::Migration
  def change
    remove_column :user, :password, :string
  end
end
