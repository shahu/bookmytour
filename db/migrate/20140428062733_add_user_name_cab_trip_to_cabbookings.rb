class AddUserNameCabTripToCabbookings < ActiveRecord::Migration
  def change
    add_column :cabbookings, :user_name, :string
    add_column :cabbookings, :cab_trip, :string
  end
end
