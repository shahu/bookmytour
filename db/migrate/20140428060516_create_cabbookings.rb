class CreateCabbookings < ActiveRecord::Migration
  def change
    create_table :cabbookings do |t|
      t.integer :cab_id
      t.string :user_id
      t.string :cab_from
      t.string :cab_to
      t.date :booking_date
      t.integer :noofperson
      t.float :amount

      t.timestamps
    end
  end
end
