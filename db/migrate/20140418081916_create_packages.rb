class CreatePackages < ActiveRecord::Migration
  def change
    create_table :packages do |t|
      t.string :title
      t.text :description
      t.float :price
      t.string :status
      t.string :type

      t.timestamps
    end
  end
end
