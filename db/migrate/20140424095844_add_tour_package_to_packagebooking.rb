class AddTourPackageToPackagebooking < ActiveRecord::Migration
  def change
    add_column :packagebookings, :tour_package, :string
  end
end
