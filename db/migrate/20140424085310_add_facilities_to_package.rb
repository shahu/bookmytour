class AddFacilitiesToPackage < ActiveRecord::Migration
  def change
    add_column :packages, :facilities, :text
  end
end
