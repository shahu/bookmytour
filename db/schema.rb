# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140428062733) do

  create_table "cabbookings", force: true do |t|
    t.integer  "cab_id"
    t.string   "user_id"
    t.string   "cab_from"
    t.string   "cab_to"
    t.date     "booking_date"
    t.integer  "noofperson"
    t.float    "amount"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "user_name"
    t.string   "cab_trip"
  end

  create_table "cabs", force: true do |t|
    t.string   "cab_name"
    t.string   "cab_type"
    t.string   "cab_number"
    t.float    "cab_rate"
    t.integer  "cab_running"
    t.string   "cab_status"
    t.string   "cab_company"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "packagebookings", force: true do |t|
    t.integer  "user_id"
    t.integer  "package_id"
    t.float    "amount"
    t.integer  "noofperson"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email"
    t.integer  "contact"
    t.date     "dob"
    t.date     "doj"
    t.string   "user_name"
    t.string   "package"
    t.text     "facilities"
    t.string   "tour_package"
  end

  create_table "packages", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.float    "price"
    t.string   "status"
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "facilities"
  end

  create_table "users", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "contact"
    t.text     "address"
    t.string   "email"
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_hash"
    t.string   "password_salt"
  end

end
