class SiteController < ApplicationController
  def index
  	 @packages = Package.all
  	 if request.xhr?
  	 	@packagebooking = Packagebooking.new(params[:packagebooking])
  	 	@success = @packagebooking.save
  	 	respond_to do |format|
	  	 	format.js
	  	 	format.html
  	 	end
  	 else
  	 	@packagebooking = Packagebooking.new
  	 	 respond_to do |format|
	  	 	format.html
  	 	end
  	 end
  end

 
end
