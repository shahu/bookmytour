class CabbookingsController < ApplicationController
  before_action :set_cabbooking, only: [:show, :edit, :update, :destroy]

  # GET /cabbookings
  # GET /cabbookings.json
  def index
    @cabbookings = Cabbooking.all
  end

  # GET /cabbookings/1
  # GET /cabbookings/1.json
  def show
  end

  # GET /cabbookings/new
  def new
    @cabbooking = Cabbooking.new
  end

  # GET /cabbookings/1/edit
  def edit
  end

  # POST /cabbookings
  # POST /cabbookings.json
  def create
    @cabbooking = Cabbooking.new(cabbooking_params)

    respond_to do |format|
      if @cabbooking.save
        format.html { redirect_to @cabbooking, notice: 'Cabbooking was successfully created.' }
        format.json { render action: 'show', status: :created, location: @cabbooking }
      else
        format.html { render action: 'new' }
        format.json { render json: @cabbooking.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cabbookings/1
  # PATCH/PUT /cabbookings/1.json
  def update
    respond_to do |format|
      if @cabbooking.update(cabbooking_params)
        format.html { redirect_to @cabbooking, notice: 'Cabbooking was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @cabbooking.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cabbookings/1
  # DELETE /cabbookings/1.json
  def destroy
    @cabbooking.destroy
    respond_to do |format|
      format.html { redirect_to cabbookings_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cabbooking
      @cabbooking = Cabbooking.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cabbooking_params
      params.require(:cabbooking).permit(:cab_id, :user_id, :user_name,:cab_from, :cab_to, :booking_date, :noofperson,:cab_trip, :amount)
    end
end
