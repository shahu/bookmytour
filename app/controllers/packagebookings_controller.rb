class PackagebookingsController < ApplicationController
  before_action :set_packagebooking, only: [:show, :edit, :update, :destroy]

  # GET /packagebookings
  # GET /packagebookings.json
  def index
    @packagebookings = Packagebooking.all
         respond_to do |format|
          format.html # show.html.erb
          format.json { render :json => @packagebooking }
        end

  end

  # GET /packagebookings/1
  # GET /packagebookings/1.json
  def show
          @packagebooking = Packagebooking.find(params[:id])
          respond_to do |format|
          format.html # show.html.erb
          #format.js
          format.json { render :json => @packagebooking }
          end
  end
   
   
  # GET /packagebookings/new
  def new
    @packagebooking = Packagebooking.new
     respond_to do |format|
          format.html # show.html.erb
          format.json { render :json => @packagebooking }
    end
        
  end

  # GET /packagebookings/1/edit
  def edit
    @packagebooking = Packagebooking.find(params[:id])
  end

  # POST /packagebookings
  # POST /packagebookings.json
  def create
    @packagebooking = Packagebooking.new(packagebooking_params)

    respond_to do |format|
      if @packagebooking.save
        format.html { redirect_to @packagebooking, notice: 'Packagebooking was successfully created.' }
        format.js
        format.json { render action: 'show', status: :created, location: @packagebooking }
      else
        format.html { render action: 'new' }
        format.js       
        format.json { render json: @packagebooking.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /packagebookings/1
  # PATCH/PUT /packagebookings/1.json
  def update
    @packagebooking = Packagebooking.find(params[:id])
    respond_to do |format|
      if @packagebooking.update(packagebooking_params)
        format.html { redirect_to @packagebooking, notice: 'Packagebooking was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @packagebooking.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /packagebookings/1
  # DELETE /packagebookings/1.json
  def destroy
    @packagebooking = Packagebooking.find(params[:id])
    @packagebooking.destroy
    respond_to do |format|
      format.html { redirect_to packagebookings_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_packagebooking
      @packagebooking = Packagebooking.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def packagebooking_params
      params.require(:packagebooking).permit(:user_id, :package_id, :user_name,:amount, :noofperson, :email, :contact, :dob, :doj,:facilities,:tour_package)
    end
end
