json.array!(@cabbookings) do |cabbooking|
  json.extract! cabbooking, :id, :cab_id, :user_id, :cab_from, :cab_to, :booking_date, :noofperson, :amount
  json.url cabbooking_url(cabbooking, format: :json)
end
