json.array!(@packages) do |package|
  json.extract! package, :id, :title, :description, :facilities, :price, :status, :type
  json.url package_url(package, format: :json)
end
