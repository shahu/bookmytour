json.array!(@packagebookings) do |packagebooking|
  json.extract! packagebooking, :id, :user_id, :usner_name,:facilities, :tour_package, :package_id, :amount, :noofperson, :email, :contact, :dob, :doj
  json.url packagebooking_url(packagebooking, format: :json)
end
