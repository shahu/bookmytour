json.array!(@cabs) do |cab|
  json.extract! cab, :id, :cab_name, :cab_type, :cab_number, :cab_rate, :cab_running, :cab_status, :cab_company
  json.url cab_url(cab, format: :json)
end
