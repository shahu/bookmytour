class Package < ActiveRecord::Base

	has_many :packagebooking
  	has_many :user, through: :packagebooking
  	attr_accessor :user_ids
end
