class Cab < ActiveRecord::Base
	has_many :cabbooking
  	has_many :user, through: :cabbooking
end
