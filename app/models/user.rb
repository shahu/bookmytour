class User < ActiveRecord::Base;
	attr_accessor :password
	before_save :encrypt_password

	validates_confirmation_of :password
	#validates_presence_of :password, :on => :create
	#validates_presence_of :email
	#validates_uniqueness_of :email

	has_many :packagebooking
  has_many :package, through: :packagebooking
  has_many :cabbooking
  has_many :cabs, through: :cabbooking
  
 

    def self.authenticate(email,password)
    	user = find_by_email(email)
    	if user && user.password == BCrypt::Engine.hash_secret(password,user.generate_salt)
    		user
    	else
    		nil
    	end
    end
  	def encrypt_password
      if password.present?
      	self.password_salt = BCrypt::Engine.generate_salt
      	self.password_salt = BCrypt::Engine.hash_secret(password,generate_salt)
      	
      end
  	end
 def user_booking

   return packagebookings.select("distinct(packagebookings.package_id), packagebookings.* ").where(:user_id => @user.id)
   
 end

end
