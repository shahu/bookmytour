require 'test_helper'

class CabsControllerTest < ActionController::TestCase
  setup do
    @cab = cabs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cabs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cab" do
    assert_difference('Cab.count') do
      post :create, cab: { cab_company: @cab.cab_company, cab_name: @cab.cab_name, cab_number: @cab.cab_number, cab_rate: @cab.cab_rate, cab_running: @cab.cab_running, cab_status: @cab.cab_status, cab_type: @cab.cab_type }
    end

    assert_redirected_to cab_path(assigns(:cab))
  end

  test "should show cab" do
    get :show, id: @cab
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cab
    assert_response :success
  end

  test "should update cab" do
    patch :update, id: @cab, cab: { cab_company: @cab.cab_company, cab_name: @cab.cab_name, cab_number: @cab.cab_number, cab_rate: @cab.cab_rate, cab_running: @cab.cab_running, cab_status: @cab.cab_status, cab_type: @cab.cab_type }
    assert_redirected_to cab_path(assigns(:cab))
  end

  test "should destroy cab" do
    assert_difference('Cab.count', -1) do
      delete :destroy, id: @cab
    end

    assert_redirected_to cabs_path
  end
end
