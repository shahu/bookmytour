require 'test_helper'

class CabbookingsControllerTest < ActionController::TestCase
  setup do
    @cabbooking = cabbookings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cabbookings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cabbooking" do
    assert_difference('Cabbooking.count') do
      post :create, cabbooking: { amount: @cabbooking.amount, booking_date: @cabbooking.booking_date, cab_from: @cabbooking.cab_from, cab_id: @cabbooking.cab_id, cab_to: @cabbooking.cab_to, noofperson: @cabbooking.noofperson, user_id: @cabbooking.user_id }
    end

    assert_redirected_to cabbooking_path(assigns(:cabbooking))
  end

  test "should show cabbooking" do
    get :show, id: @cabbooking
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cabbooking
    assert_response :success
  end

  test "should update cabbooking" do
    patch :update, id: @cabbooking, cabbooking: { amount: @cabbooking.amount, booking_date: @cabbooking.booking_date, cab_from: @cabbooking.cab_from, cab_id: @cabbooking.cab_id, cab_to: @cabbooking.cab_to, noofperson: @cabbooking.noofperson, user_id: @cabbooking.user_id }
    assert_redirected_to cabbooking_path(assigns(:cabbooking))
  end

  test "should destroy cabbooking" do
    assert_difference('Cabbooking.count', -1) do
      delete :destroy, id: @cabbooking
    end

    assert_redirected_to cabbookings_path
  end
end
