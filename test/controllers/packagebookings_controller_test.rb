require 'test_helper'

class PackagebookingsControllerTest < ActionController::TestCase
  setup do
    @packagebooking = packagebookings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:packagebookings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create packagebooking" do
    assert_difference('Packagebooking.count') do
      post :create, packagebooking: { amount: @packagebooking.amount, contact: @packagebooking.contact, dob: @packagebooking.dob, doj: @packagebooking.doj, email: @packagebooking.email, noofperson: @packagebooking.noofperson, package_id: @packagebooking.package_id, user_id: @packagebooking.user_id }
    end

    assert_redirected_to packagebooking_path(assigns(:packagebooking))
  end

  test "should show packagebooking" do
    get :show, id: @packagebooking
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @packagebooking
    assert_response :success
  end

  test "should update packagebooking" do
    patch :update, id: @packagebooking, packagebooking: { amount: @packagebooking.amount, contact: @packagebooking.contact, dob: @packagebooking.dob, doj: @packagebooking.doj, email: @packagebooking.email, noofperson: @packagebooking.noofperson, package_id: @packagebooking.package_id, user_id: @packagebooking.user_id }
    assert_redirected_to packagebooking_path(assigns(:packagebooking))
  end

  test "should destroy packagebooking" do
    assert_difference('Packagebooking.count', -1) do
      delete :destroy, id: @packagebooking
    end

    assert_redirected_to packagebookings_path
  end
end
